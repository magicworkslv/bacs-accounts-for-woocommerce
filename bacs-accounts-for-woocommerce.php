<?php
/*
  Plugin Name:  Bacs Accounts for WooCommerce
  Description: Allows adding several bank accounts for direct deposit and connect with products
  Version: 1.1
  Author: Laura Lieknina
  Author URI: http://magicworks.lv
  License: GPLv2+
  Text Domain: direct-deposit-for-woocommerce
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	/**
	 * Custom Payment Gateway.
	 *
	 * Provides a Custom Payment Gateway, to modify BACS formatting.
	 */
	add_action('plugins_loaded', 'init_custom_gateway_class');

	function init_custom_gateway_class(){

	    class WC_Gateway_Direct_Deposit extends WC_Payment_Gateway {


			/**
			 * Constructor for the gateway.
			 */
			public function __construct() {

				$this->id                 = 'direct_deposit';
				$this->icon               = apply_filters('woocommerce_bacs_icon', '');
				$this->has_fields         = false;
				$this->method_title       = __( 'Direct Deposit for Various Sellers', 'woocommerce' );
				$this->method_description = __( 'Allows payments by BACS, more commonly known as direct bank/wire transfer.', 'woocommerce' );

				// Load the settings.
				$this->init_form_fields();
				$this->init_settings();

				// Define user set variables
				$this->title        = $this->get_option( 'title' );
				$this->description  = $this->get_option( 'description' );
				$this->instructions = $this->get_option( 'instructions', $this->description );
				$this->message1 	= $this->get_option( 'message1');
				$this->message2 	= $this->get_option( 'message2');

				// BACS account fields shown on the thanks page and in emails
				$this->account_details = get_option( 'woocommerce_bacs_accounts',
					array(
						array(
							'account_name'   		=> $this->get_option( 'account_name' ),
							'account_vat_number'  	=> $this->get_option( 'account_vat_number' ),
							'account_number' 		=> $this->get_option( 'account_number' ),
							'sort_code'      		=> $this->get_option( 'sort_code' ),
							'bank_name'      		=> $this->get_option( 'bank_name' ),
							'iban'           		=> $this->get_option( 'iban' ),
							'bic'            		=> $this->get_option( 'bic' )
						)
					)
				);

				// Actions
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'save_account_details' ) );
				add_action( 'woocommerce_thankyou_direct_deposit', array( $this, 'thankyou_page' ) );

				// Customer Emails
				add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
			}

			/**
			 * Initialise Gateway Settings Form Fields.
			 */
			public function init_form_fields() {

				$this->form_fields = array(
					'enabled' => array(
						'title'   => __( 'Enable/Disable', 'woocommerce' ),
						'type'    => 'checkbox',
						'label'   => __( 'Enable Bank Transfer', 'woocommerce' ),
						'default' => 'no'
					),
					'title' => array(
						'title'       => __( 'Title', 'woocommerce' ),
						'type'        => 'text',
						'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
						'default'     => __( 'Direct Bank Transfer', 'woocommerce' ),
						'desc_tip'    => true,
					),
					'description' => array(
						'title'       => __( 'Description', 'woocommerce' ),
						'type'        => 'textarea',
						'description' => __( 'Payment method description that the customer will see on your checkout.', 'woocommerce' ),
						'default'     => __( 'Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won\'t be shipped until the funds have cleared in our account.', 'woocommerce' ),
						'desc_tip'    => true,
					),
					'instructions' => array(
						'title'       => __( 'Instructions', 'woocommerce' ),
						'type'        => 'textarea',
						'description' => __( 'Instructions that will be added to the thank you page and emails.', 'woocommerce' ),
						'default'     => '',
						'desc_tip'    => true,
					),
					'message1' => array(
						'title'       => __( 'Message', 'woocommerce' ),
						'type'        => 'textarea',
						'description' => __( 'Instructions that will be added in case there will be no bank account connected to product.', 'woocommerce' ),
						'default'     => __( 'Instructions that will be added in case there will be no bank account connected to product.', 'woocommerce' ),
						'desc_tip'    => true,
					),
					'message2' => array(
						'title'       => __( 'Message', 'woocommerce' ),
						'type'        => 'textarea',
						'description' => __( 'Instructions that will be added in case there will be several products pruchased from different sellers.', 'woocommerce' ),
						'default'     => __( 'Instructions that will be added in case there will be several products pruchased from different sellers.', 'woocommerce' ),
						'desc_tip'    => true,
					),
					'account_details' => array(
						'type'        => 'account_details'
					),
				);

			}

			/**
			 * Generate account details html.
			 *
			 * @return string
			 */
			public function generate_account_details_html() {

				ob_start();

				?>
				<tr valign="top">
					<th scope="row" class="titledesc"><?php _e( 'Account Details', 'woocommerce' ); ?>:</th>
					<td class="forminp" id="bacs_accounts">
						<table class="widefat wc_input_table sortable" cellspacing="0">
							<thead>
								<tr>
									<th class="sort">&nbsp;</th>
									<th><?php _e( 'Account Name (Company Name)', 'woocommerce' ); ?></th>
									<th><?php _e( 'VAT Number', 'woocommerce' ); ?></th>
									<th><?php _e( 'Account Number', 'woocommerce' ); ?></th>
									<th><?php _e( 'Bank Name', 'woocommerce' ); ?></th>
									<th><?php _e( 'IBAN', 'woocommerce' ); ?></th>
									<th><?php _e( 'BIC / Swift', 'woocommerce' ); ?></th>
								</tr>
							</thead>
							<tbody class="accounts">
								<?php
								$i = -1;
								if ( $this->account_details ) {
									foreach ( $this->account_details as $account ) {
										$i++;

										echo '<tr class="account">
											<td class="sort"></td>
											<td><input type="text" value="' . esc_attr( wp_unslash( $account['account_name'] ) ) . '" name="bacs_account_name[' . $i . ']" /></td>
											<td><input type="text" value="' . esc_attr( wp_unslash( $account['account_vat_number'] ) ) . '" name="bacs_account_vat_number[' . $i . ']" /></td>
											<td><input type="text" value="' . esc_attr( $account['account_number'] ) . '" name="bacs_account_number[' . $i . ']" /></td>
											<td><input type="text" value="' . esc_attr( wp_unslash( $account['bank_name'] ) ) . '" name="bacs_bank_name[' . $i . ']" /></td>
											<td><input type="text" value="' . esc_attr( $account['iban'] ) . '" name="bacs_iban[' . $i . ']" /></td>
											<td><input type="text" value="' . esc_attr( $account['bic'] ) . '" name="bacs_bic[' . $i . ']" /></td>
										</tr>';
									}
								}
								?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="7"><a href="#" class="add button"><?php _e( '+ Add Account', 'woocommerce' ); ?></a> <a href="#" class="remove_rows button"><?php _e( 'Remove selected account(s)', 'woocommerce' ); ?></a></th>
								</tr>
							</tfoot>
						</table>
						<script type="text/javascript">
							jQuery(function() {
								jQuery('#bacs_accounts').on( 'click', 'a.add', function(){

									var size = jQuery('#bacs_accounts').find('tbody .account').length;

									jQuery('<tr class="account">\
											<td class="sort"></td>\
											<td><input type="text" name="bacs_account_name[' + size + ']" /></td>\
											<td><input type="text" name="bacs_account_vat_number[' + size + ']" /></td>\
											<td><input type="text" name="bacs_account_number[' + size + ']" /></td>\
											<td><input type="text" name="bacs_bank_name[' + size + ']" /></td>\
											<td><input type="text" name="bacs_iban[' + size + ']" /></td>\
											<td><input type="text" name="bacs_bic[' + size + ']" /></td>\
										</tr>').appendTo('#bacs_accounts table tbody');

									return false;
								});
							});
						</script>
					</td>
				</tr>
				<?php
				return ob_get_clean();

			}

			/**
			 * Save account details table.
			 */
			public function save_account_details() {

				$accounts = array();

				if ( isset( $_POST['bacs_account_name'] ) ) {

					$account_names   = array_map( 'wc_clean', $_POST['bacs_account_name'] );
					$account_vat_numbers   = array_map( 'wc_clean', $_POST['bacs_account_vat_number'] );
					$account_numbers = array_map( 'wc_clean', $_POST['bacs_account_number'] );
					$bank_names      = array_map( 'wc_clean', $_POST['bacs_bank_name'] );
					$sort_codes      = array_map( 'wc_clean', $_POST['bacs_sort_code'] );
					$ibans           = array_map( 'wc_clean', $_POST['bacs_iban'] );
					$bics            = array_map( 'wc_clean', $_POST['bacs_bic'] );

					foreach ( $account_names as $i => $name ) {
						if ( ! isset( $account_names[ $i ] ) ) {
							continue;
						}

						$accounts[] = array(
							'account_name'       	=> $account_names[ $i ],
							'account_vat_number' 	=> $account_vat_numbers[ $i ],
							'account_number' 		=> $account_numbers[ $i ],
							'bank_name'      		=> $bank_names[ $i ],
							'sort_code'      		=> $sort_codes[ $i ],
							'iban'           		=> $ibans[ $i ],
							'bic'            		=> $bics[ $i ]
						);
					}
				}

				update_option( 'woocommerce_bacs_accounts', $accounts );

			}

			/**
			 * Output for the order received page.
			 *
			 * @param int $order_id
			 */
			public function thankyou_page( $order_id ) {

				if ( $this->instructions ) {
					echo wpautop( wptexturize( wp_kses_post( $this->instructions ) ) );
				}
				$this->bank_details( $order_id );

			}

			/**
			 * Add content to the WC emails.
			 *
			 * @param WC_Order $order
			 * @param bool $sent_to_admin
			 * @param bool $plain_text
			 */
			public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {

				if ( ! $sent_to_admin && 'direct_deposit' === $order->payment_method && $order->has_status( 'on-hold' ) ) {
					if ( $this->instructions ) {
						echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
					}
					$this->bank_details( $order->id );
				}

			}

			/**
			 * Get bank details and place into a list format.
			 *
			 * @param int $order_id
			 */
			private function bank_details( $order_id = '' ) {

				if ( empty( $this->account_details ) ) {
					return;
				}

				// Get order and store in $order
				$order 		= wc_get_order( $order_id );

				$bacs_accounts = apply_filters( 'woocommerce_bacs_accounts', $this->account_details );

				if ( ! empty( $bacs_accounts ) ) {

					echo '<h2 class="wc-bacs-bank-details-heading">' . __( 'Our Bank Details', 'woocommerce' ) . '</h2>' . PHP_EOL;

					// Show seller bacs corresponding to cart items
					$bacs = array();
					$products = 0;

					foreach ($order->get_items() as $item_key => $item_values):

					    $product_id = $item_values->get_product_id(); // the Product id []
					    // populate array with bacs connected to products in order
					    if (get_post_meta($product_id, '_wc_bacs_data', true) != '')
						    $bacs[] = get_post_meta($product_id, '_wc_bacs_data', true);

						$products++;

					endforeach;

					$bacs = array_unique($bacs);

					if (count($bacs) > 0 &&  count($bacs) == $products) {
						if ( count($bacs) == 1) {
							// there is one seller for all products in order list, so print out details of that one
							foreach ( $bacs_accounts as $bacs_account ) {

								$bacs_account = (object) $bacs_account;

								if ($bacs[0] == $bacs_account->account_number) {

									echo '<ul class="wc-bacs-bank-details order_details bacs_details">' . PHP_EOL;

									// PAYMENT DETAILS

									// BACS account fields shown on the thanks page and in emails
									$account_fields = apply_filters( 'woocommerce_bacs_account_fields', array(
										'bank_name'=> array(
											'label' => __( 'Bank Name', 'woocommerce' ),
											'value' => $bacs_account->bank_name
										),
										'account_name'=> array(
											'label' => __( 'Account Name', 'woocommerce' ),
											'value' => $bacs_account->account_name
										),
										'account_vat_number'=> array(
											'label' => __( 'VAT Number', 'woocommerce' ),
											'value' => $bacs_account->account_vat_number
										),
										'account_number'=> array(
											'label' => __( 'Account Number', 'woocommerce' ),
											'value' => $bacs_account->account_number
										),
										'iban'          => array(
											'label' => __( 'IBAN', 'woocommerce' ),
											'value' => $bacs_account->iban
										),
										'bic'           => array(
											'label' => __( 'BIC', 'woocommerce' ),
											'value' => $bacs_account->bic
										)
									), $order_id );

									foreach ( $account_fields as $field_key => $field ) {
										if ( ! empty( $field['value'] ) ) {
											echo '<li class="' . esc_attr( $field_key ) . '">' . esc_attr( $field['label'] ) . ': <strong>' . wptexturize( $field['value'] ) . '</strong></li>' . PHP_EOL;
										}
									}

									echo '</ul>';

								}
							}
						}
						else {
							//there are several several sellers and several bacs needs to be shown
							echo '<p>';
						 	echo $this->message2;
						 	echo '</p>';

							$products_by_bac = array();

							foreach ($order->get_items() as $item_key => $item_values):

								// Here you can print out the product list grouped by sellers.

							    // $product_id = $item_values->get_product_id(); // the Product id []
							    // $bacs[] = get_post_meta($product_id, '_wc_bacs_data', true);
							    // $product = $item_values->get_product(); // the WC_Product object

							endforeach;

						}
					}
					else {
						//bacs not assigned to all or some products
						echo '<p>';
					 	echo $this->message1;
					 	echo '</p>';
					}

				}

			}

			/**
			 * Process the payment and return the result.
			 *
			 * @param int $order_id
			 * @return array
			 */
			public function process_payment( $order_id ) {

				$order = wc_get_order( $order_id );

				// Mark as on-hold (we're awaiting the payment)
				$order->update_status( 'on-hold', __( 'Awaiting BACS payment', 'woocommerce' ) );

				// Reduce stock levels
				$order->reduce_order_stock();

				// Remove cart
				WC()->cart->empty_cart();

				// Return thankyou redirect
				return array(
					'result'    => 'success',
					'redirect'  => $this->get_return_url( $order )
				);

			}

		}

		add_filter( 'woocommerce_payment_gateways', 'add_direct_deposit' );
		function add_direct_deposit( $methods ) {
		    $methods[] = 'WC_Gateway_Direct_Deposit';
		    return $methods;
		}


		// Create metabox to choose direct deposit account for a product
		function add_wc_bacs_dropdown()
		{
		    $screens = ['product'];
		    foreach ($screens as $screen) {
		        add_meta_box(
		            'wc_bacs_metabox_id',     // Unique ID
		            'Product seller account', // Box title
		            'wc_bacs_metabox_html',   // Content callback, must be of type callable
		            $screen                   // Post type
		        );
		    }
		}
		add_action('add_meta_boxes', 'add_wc_bacs_dropdown');

		function wc_bacs_metabox_html($post)
		{
			$account_details = get_option( 'woocommerce_bacs_accounts',
				array(
					array(
						'account_name'   		=> get_option( 'account_name' ),
						'account_number'   		=> get_option( 'account_number' ),
					)
				)
			);

			$value = get_post_meta($post->ID, '_wc_bacs_data', true);

		    ?>
		    <label for="wc_bacs_field">Please choose the seller account for the product.</label><br /><br />
		    <select name="wc_bacs_field" id="wc_bacs_field" class="postbox">
		    	<option value="">Select product seller account</option>
		        <?php foreach ($account_details as $key => $account) {
		        	echo '<option value="'.$account['account_number'].'"'.($account['account_number']==$value?'selected=selected':'').'>'.$account['account_name'].'</option>';
		        } ?>
		    </select>
		    <?php
		}

		function wc_bacs_metabox_save_postdata($post_id)
		{
		    if (array_key_exists('wc_bacs_field', $_POST)) {
		        update_post_meta(
		            $post_id,
		            '_wc_bacs_data',
		            $_POST['wc_bacs_field']
		        );
		    }
		}
		add_action('save_post', 'wc_bacs_metabox_save_postdata');
	}
}